### 巴巴罗萨的归来（9）

#### Frederick’s Promotion of Economic Development 巴巴罗萨对经济发展的促进

巴巴罗萨在1165年造访德意志经济最发达的莱茵河下游地区之前，似乎对阿尔卑斯山以北地区的经济事务并不感兴趣，也许是因为在此之前他作为君主却基本上都不在国境内。正如我们已经知道的，1155年他从意大利回来后就取消了美因河上未经授权的收费，除了帝国在法兰克福征收的通行费和圣母升天日（8月15日）前后7天在诺伊施塔特（Neustadt）和阿沙芬堡对每艘新船征收4便士之外，由水路或是沿河的皇家道路逆流而上的商人们将被免除一切通行费。

巴巴罗萨在1165年之前采取的另一项行动也带来了经济上的影响。他于1157年4月6日确认了他的外曾祖父亨利四世在1090年左右授予沃尔姆斯的犹太人的一项失去的特权。这项特权将犹太人通过世袭权力继承的所有财产，无论是不动产还是个人资产，还是土地或是人力资源，都置于国王的保护之下，并授予他们在整个王国无需支付通行费或其他任何公共或私人税收便可进行贸易的权利。他们可以在整个沃尔姆斯城里担任货币兑换员，但在铸币厂和专属货币兑换员交换货币的地方除外。犹太人不可以将基督徒作为奴隶，但可以雇佣基督徒当女仆或奶妈。他们不能被强迫为国王或是主教的事业做贡献，也不能违背他们的意愿就侵占他们的住宅。同时特权还禁止强迫犹太儿童接受基督徒的洗礼，但如果他们自愿改换信仰，就会失去父亲的遗产，就如同他们放弃遵守父辈的法律了一样。犹太人要按照他们自己的法律体系进行审判，即便是在涉及基督徒的案件中也是如此。很难说巴巴罗萨是否只是重新确认了他外曾祖父给予犹太人的特权，即使这种特权事实上已经被其他的做法所取代，我们也不清楚该宪章是否准确描述了在12世纪中期，犹太人在德意志地区的生活状况还相对比较优越。

然而尽管巴巴罗萨宣称犹太人归皇室管理，但他的意思也只是宣称了自己的领主权，既包括保护犹太人及其财产。该宪章对犹太人的个人法律地位却只字未提，也就是，我们也不知道他们是自由民还是被奴役的；我们不应将巴巴罗萨在这里提出的主张与1236年腓特烈二世在宪章中首次出现的犹太商会农奴制度相提并论。

巴巴罗萨大约是在去了意大利三次之后对商业交易的重要性有了更加深刻的认识，在1165年的秋天他在这方面变得积极主动起来。9月24日，他确认了沃尔姆斯的货币商人的权利。一个官员组成的联盟负责管理铸币厂。 新成员需要向主教提供半盎司，即14.61克的黄金，并向铸币师和主教的侍从官提供一个金便士或60个银便士。铸币厂的利润属于主教，但他必须向铸币师支付3先令的工资，向其他劳工支付6便士。除了犹太人之外，只有货币商人才有资格在城里兑换货币。（这一规定也侧面体现，巴巴罗萨在1157年对犹太人许诺的特权也并非一纸空文。）当皇帝或罗马人国王来到沃尔姆斯时，他可以向货币商人支付他希望铸成硬币的硬块，而商人不能截留任何银块或硬币作为自己的利润。在这种访问期间，如果主教没有足够的官员来供给宫廷的日常生活，他也可以雇佣铸币师来作为皇家的各类侍从，因为宪章中规定了，铸币厂也是皇室的一部分；但这种雇佣也不能违背铸币师的意愿强迫他们在市政办公室工作。

虽然铸造硬币是皇室的特权，但发行硬币的特权已经通过皇室的层层授予或剥夺，转移到了教会或是非教会诸侯手中。在12世纪上半夜，德意志地区只有大约20多个活跃的铸币厂，其中主要的，如沃尔姆斯的铸币厂都掌握在主教们的手中，他们是王国中统治很多重要城市的领主。在洛塔尔三世和康拉德三世统治时期，只有戈斯拉尔和纽伦堡的皇家铸币厂在运转。而到了1197年，既亨利六世去世时，全国共有215个铸币厂，其中61个属于主教，45个属于修道院，81个属于其他普通领主，还有28个直接属于国王。其中有12个皇家铸币厂，包括亚琛、多瑙沃特、乌尔姆、阿格诺、杜伊斯堡、凯泽斯沃特（Kaiserswerth）、法兰克福、盖尔恩豪森和多特蒙德的铸币厂，都是巴巴罗萨统治时期建立的。铸币厂数量的激增证明了巴巴罗萨统治时期经济发生了巨大增长，但也不好说他个人对于推动这一发展起到了多大作用。

1165年11月，应乌得勒支主教还有荷兰、海尔德（Guelders）、克莱维斯（Cleves）等伯爵的要求，巴巴罗萨在乌得勒支访问期间，授权在乌得勒支附近的瑙达（Neuda）地区的河流和北海之间修建一条运河。他还下令拆除了荷兰伯爵擅自修建的堤坝，这样就不会有障碍物阻碍莱茵河的流动了。

在凯泽斯沃特的新宫殿动工后（大约是在1158年），帝国宫廷不再在杜伊斯堡的前加洛林王朝时期的庄园停留（该庄园位于鲁尔河和莱茵河的交汇处，在科隆的下游）；但杜伊斯堡的定居点还是因其处于连接莱茵河与威悉河的海尔维格（Hellweg）公路的终点而发展了起来，并且因莱茵河流域与英格兰之间贸易的日益增长而更加繁荣。在1165年亚琛给查理曼封圣的圣诞节集会上，巴巴罗萨还免除了乌得勒支主教戈弗雷（Bishop Godfrey）强加给杜伊斯堡居民的非法义务，除了他们因使用了主教的设施而亏欠的1便士。可想而知，杜伊斯堡的商人应该是在皇帝在11月造访乌得勒支的时候向他抱怨过主教的暴政。

亚琛的圣诞集会上的另一位前来向巴巴罗萨致意的与会者是弗兰德斯的菲利普（Philip of Flanders），并且巴巴罗萨授予了他康布雷地区。作为秃头查理（Charles the Bald）的女儿朱迪丝的后裔，菲利普可能也因为查理曼大帝被封圣而获得了一些家族利益。在菲利普停留期间，巴巴罗萨授予弗兰德斯的商人在帝国各地自由通行的权利，因为科隆一直在试图阻止根特（Ghent）的商人在莱茵河上游将商品运到科隆以外的地方。
