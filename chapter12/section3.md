### 追捕教宗亚历山大三世（3）

被路易以这种戏剧性的方式拒绝的情况下，巴巴罗萨和雷纳德还在试图尽可能地挽救局势。一位匿名者和亲亚历山大三世一派的丹麦编年史学家萨克索·格拉玛提库斯（Saxo Grammaticus，约于1220年去世）的记载是1162年9月7日发生事件的主要资料来源，他们指出维克多四世在会议开幕时便宣布，之所以他才是合法的教宗，是因为他的选举程序是正当的，而且与亚历山大三世不同，他愿意将此事提交给议会裁决。之后巴巴罗萨在一个简短的讲话中宣布，他已经邀请“各个省份的国王（provinciarum reges）”来参加会议，以免被认为完全忽视了他们的意见。然而裁决有争议的教宗选举是皇帝的专属特权，因为罗马位于帝国境内，而其他国王对其辖区外的主教缺乏管辖权。巴巴罗萨的论点在于，教宗和帝国其他主教一样，是从皇帝那里被授予权利的。但这样一来，如果他真的提出了这个论点，他反而是有些微妙地削弱了先前他提出的帝国对整个世界都有着主宰权的主张。如果教宗只是罗马的主教，那么他的管辖范围就会仅仅限于意大利的行省，而并非普世的教宗，于是巴巴罗萨作为罗马教会的保护者的权威也同样被削弱了，皇帝的地位就降到了和欧洲其他君主一样，只是自己帝国的统治者。

根据丹麦历史学家的说法，雷纳德在会议的总结性发言中表示，“各行省的国王”如果质疑皇帝的公正性便是一种冒犯，如果他们认为皇帝干预位于他们内的主教选举争议是对他们自己权益的冒犯，那么他们现在来干涉罗马教宗的选举其实也是不正当的。为了确保所有与会者都能够听懂他的话，雷纳德先是用拉丁语，然后再用了法语与德语发表演讲。不出所料，维克多四世再次被裁定为合法教宗，而亚历山大三世被开除教籍。巴巴罗萨又恢复了自己原先的政策，用自己的权威强行将自己支持的教宗扶上台。

人们很容易把“行省国王（provincial kings）”这种对欧洲其他君主的蔑称当做是与帝国敌对的丹麦编年史学家大约在1200年左右的发明创造，但帝国公证人，科隆的伯查德在1161-1162年之交与锡格堡的尼古拉斯院长的信中写道，“所有的国王（reguli）之所以不敢承认‘枢机主教罗兰’作为教宗，更多是出于对皇帝威势的恐惧，而并非站在正义一方。”这种使帝国具有优越性的话术在科隆大主教周围人的圈子里十分盛行。和米兰战败后歌颂巴巴罗萨为“世界之主”的“皇帝赞歌”一样，对欧洲其他君主的蔑称也经常被引用为霍亨斯陶芬家族在巴巴罗萨统治时期处于力量的巅峰，宣称自己拥有整个世界的统治权的证据。鉴于皇帝最终还是未能说服路易也承认维克多四世，所以这段话听起来只不过是一种苍白无力的虚张声势，就像一个被宠坏的孩子因为自己的诉求没有得到满足而在胡乱发脾气。

几年后，帕绍的赫尔莫德在记载自己对圣·让-德-洛斯内会议最终破产的简要描述时是这样写的：“……精明的法国人以其聪明才智获得了军事力量无法实现的利益。自此之后，教宗亚历山大三世获得了更大的影响力。”这也是对巴巴罗萨与雷纳德外交失败的严厉控诉。

即便是巴巴罗萨在圣·让-德-洛斯内看似获得的一个成功也最终被证明是短暂的。丹麦国王瓦尔德马一世时唯一出席会议的其他国家君主，他在9月7日代表丹麦王国向巴巴罗萨致敬，正如他的使节在1158年6月于奥格斯堡所承诺的那样。然而到了1182年，巴巴罗萨的权威因1177年在威尼斯与亚历山大三世拖鞋，以及在1180年无力保护狮子亨利不受其敌人侵害而严重受损，瓦尔德马之子，丹麦王国的继承人克努特六世（Knut VI, r. 1182-1202）拒绝臣服于巴巴罗萨成为其封建封臣。据萨克索·格拉玛提库斯记载（其编年史便是为了庆祝丹麦脱离帝国宗主国身份所作），隆德大主教阿布萨隆（Archbishop Absalon of Lund）告知巴巴罗萨的使节，克努特同样有权利来管理自己的王国，就像皇帝在统治罗马帝国一样。巴巴罗萨和雷纳德在圣·让-德-洛斯内的言论已经完全被推翻了，国王现在与皇帝一样，都是平等的统治者了。

1162年后，亚历山大三世多次向巴巴罗萨示好。9月18日，教宗邀请萨尔茨堡的埃伯哈德或者其代表前去会见自己，并授权大主教如果可以的话去拜访一下皇帝，设法帮助他恢复教会的统一；埃伯哈德和布里克森的哈特曼主教很可能为此参加了1163年4月在美因茨举行的帝国集会。1163年5月19日的图尔会议上，亚历山大三世重申了对维克多四世以及其支持者们开除教籍的意见，但却并没有明确将巴巴罗萨纳入被谴责的范畴。1163年8月，教宗的使节在纽伦堡访问了帝国共同，向他保证教宗没有任何反对帝国的阴谋，但巴巴罗萨依然拒绝接见亚历山大三世的枢机主教，他只接待了其它使节。他们同意挑选两名教会中的中立人士，再次组成一个7人的仲裁委员会来解决教会分裂问题。这个计划之后再无下文。巴巴罗萨必须承认的是，他1160年在帕维亚、1161年在洛迪和1162年在圣·让-德-洛斯内所奠定的基调是完全错误的，他没法不追求潜在的与亚历山大三世和解的机会，而这对帝国和其个人的威望都是巨大的打击。

#### The Punishment of Mainz 对美因茨的惩戒

巴巴罗萨自1162年10月回到德意志地区短暂停驻一年后返回意大利。在这期间他的主要活动是调查并处置美因茨大主教阿诺德于1160年6月24日被谋杀的事件。1153年巴巴罗萨将大主教亨利罢黜后，曾将美因茨的官员塞伦霍芬的阿诺德提拔为圣博尼法斯大主教，后者还曾是他的第一任宫相。阿诺德收回其被教廷剥夺的土地的政策，以及他卑微的出身和暴躁易怒的性格，引起了莱茵兰地区贵族们的反对。巴巴罗萨曾在1155年的圣诞机会上判处斯塔莱克的赫尔曼与阿诺德扰乱和平的罪行，并除以他们牵着狗游行的惩罚。

1155年后再起冲突的原因是阿诺德对美因茨地区没有参加巴巴罗萨的第二次意大利战争的官员还有乡绅们征收额外的赋税，而他们拒绝缴纳。1158年第一次围攻米兰时，诸侯们针对阿诺德提起的诉讼给出的裁定是没收那些不遵守规定的诸侯们的土地。在阿诺德缺席的时候，大主教在塞伦霍芬城里的对手梅因戈特（Meingote）家族领导了一场反对大主教的起义，大主教在回国时不得不使用武力强行攻陷美因茨。他流放了几名起义者，而这些人试图到意大利为自己申辩。巴巴罗萨命令阿诺德在他们进行适当赔偿后便赦免他们。而当阿诺德拒绝接受他们愿意给出的赔偿时，一群暴徒入侵了大教堂并劫掠了大主教的官邸。1159年11月1日，大主教被起义的领袖宣布开除教籍，并且逐出美因茨教区。他回到了意大利，而此时巴巴罗萨需要阿诺德支持他在教会分裂事件中的立场，在克雷马城外的帝国营地以最高规格接待了他。美因茨的贵族们受到了谴责，巴巴罗萨命令他们对其造成的损失进行赔偿。

阿诺德在帝国特使的护送下于1160年4月1日左右抵达美因茨城外，并等待着市民们的归顺。有些人屈服了，但还有些被流放的反对者又回到了城市再度掀起叛逆乱。阿诺德向其他诸侯们求援，并从自己的大主教领调集军队。之后美因茨城中告知阿诺德他们准备投降，6月23日，大主教屏退大部分随从，住进了从山上俯瞰该城的圣詹姆斯修道院，等待美因茨进行投降仪式。第二天谈判破裂了，但戈弗雷修士向阿诺德保障了其生命安全，于是他仍然留在修道院。6月24日，市民们在梅因戈特的带领下闯进院子了，纵火焚烧了修道院。一名骑士发现了躲藏起来的阿诺德，用剑柄击打他的太阳穴将其杀死。他的尸体被砍得粉碎。

作为皇帝，巴巴罗萨有义务惩戒这种相当恶劣的破坏和平的行为。他大约从1163年3月31日至4月18日都一直待在美因茨处理这事。起义的领导人和杀害阿诺德的凶手都逃走了。4月7日的法庭上，有罪的人被永久流放，他们的财产被没收、房屋被拆毁，但仅有一名乡绅被当场处决。修道院长戈弗雷被剥夺了职务后放逐，圣詹姆斯修道院的编制被解散。美因茨的城墙和塔楼被夷为平地，护城河被填平，而城市的特权也被取消，美因茨自此被剥夺了法律上的特权地位。这是一个相当严厉的惩罚，但比起托尔托纳、斯波莱托以及米兰还是要轻上一些。奇怪的是，与阿诺德敌对的许多教士、官员们很快便被皇帝赦免。巴巴罗萨可能认为，尽管他有义务为大主教被谋杀一事报仇，但总是惹出麻烦的阿诺德并不是一个好的人选；大主教的世俗权威被削弱使得巴巴罗萨能够以美因茨为代价，反而扩张了帝国的领地。
