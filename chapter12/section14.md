### 追捕教宗亚历山大三世（14）

6月的某个时候，巴巴罗萨得知威廉二世正在围攻一座帝国的要塞。于是他把贝阿特丽丝和步兵都留下，只带着骑士匆匆地赶往那里。威廉闻讯撤退，而巴巴罗萨则在他的一些随从们试图渡过一条小河时抓住了他们。皇帝加固了城堡并占领了另一座城堡，还对特龙托河（Tronto）沿岸的领土进行了破坏和掠夺。特龙托河是安科纳与诺曼王国在亚得里亚海沿岸的分界线，这也是巴巴罗萨唯一一次踏入西西里诺曼王国的领地。他最终回应了还停留在维泰博的帕斯卡尔三世，不再拖延，而是决定立即将亚历山大三世逐出罗马。巴巴罗萨和帕斯卡尔大约在7月19、22或24日抵达了罗马城外，皇帝在马里奥山（Monte Mario）附近的老地方扎营。

帝国军队击败了维里达利亚门（Porta Viridaria，利奥城墙（Leonine City）的一个门）的守军；罗马人退到了台伯河对岸。巴巴罗萨没能拿下亚历山大主力部队缩在的圣安杰洛城堡（Castel S. Angelo）；他用了好几天时间——至少从7月24日至29日来围攻防守严密的圣彼得大教堂。据博索说，由于皇帝兵力不足或者他并不倾向对该城进行全面而持久的围攻，他开始与枢机主教们还有罗马人民谈判。美因茨的康拉德——现在是萨比纳的枢机主教——充当了中间人。如果枢机主教们能够劝说亚历山大辞职，那么巴巴罗萨表示他也会敦促帕斯卡尔这样做，并且保证不干涉随后的新选举。不出所料，枢机主教们拒绝了这一提议，因为他们认为教宗不应受任何世俗判断的约束。而且很难相信巴巴罗萨会在胜利已经唾手可得的时候真的准备放弃帕斯卡尔。假设博索的记载是真的，那么巴巴罗萨的真实目的可能是让罗马人去反对亚历山大，因为亚历山大宁愿让他们被围困也不愿辞去教宗之位。在7月下旬的某个时刻（时间线的顺序并不确定），亚历山大意识到他的处境已经无法维系。他伪装成朝圣者逃离了罗马，前往南方160英里处的贝内文托。巴巴罗萨的猎物已经从他手中溜走了。

7月29日，巴巴罗萨的人放火烧毁了与圣彼得大教堂相邻的位于图里（Turri）的圣玛丽亚教堂。据《莫雷纳编年史》的续编者说，有几件无价之宝毁于这场浩劫：一幅独一无二的用纯金制成，装饰华丽的基督像，还有一幅用同样材料和相似工艺制成的精美的圣彼得像。大火蔓延到了圣彼得大教堂的中庭和大门，于是守军投降了。韦尔夫六世在这些事件发生后立刻从圣地返回，他诅咒他的侄子和军队犯下这样的亵渎罪行——或者说，《韦尔夫家族史》的作者在事后将这个带有预见性的诅咒归在了公爵的名义下？7月30日，巴巴罗萨在圣彼得大教堂为帕斯卡尔举行了加冕仪式，作为回报，教宗也亲自为皇帝戴上金冠。应巴巴罗萨的要求，帕斯卡尔还为15名教士举行了祝圣仪式，其中包括贝桑松大主教赫里伯特（Archbishop Heribert of Besançon）、富尔达修道院院长赫尔曼，还有巴塞尔及斯特拉斯堡的主教们。两天后，即圣彼得受铁链刑日（the feast of St. Peter in Chains/Saint Peter adVincula），帕斯卡尔为贝阿特丽丝与巴巴罗萨加冕。使皇帝的胜利美中不足的只有亚历山大三世的逃脱。

当巴巴罗萨攻克利奥城墙时，除了圣安杰洛城堡外，包括拉特兰宫在内的建筑群以及罗马的城市中心其实都位于台伯河的对岸。巴巴罗萨急于与罗马人民达成协议，这样他就可以在教宗的宫殿里加冕帕斯卡尔，使得他作为合法教宗的地位更加显著，并对诺曼人发起他已经推迟了很久的进攻。8月2日，一搜比萨战舰的到来说服了罗马人，他们在图斯库鲁姆战役失败后一直在努力与帝国保持和平，并于巴巴罗萨达成了谅解。皇帝宣布协议条款的金玺诏书并没有保存下来，但他与罗马人民之间达成的具有约束力的口头协议的书面摘要被收录进了《科隆皇家编年史》。消息的来源毫无疑问是宫相海因斯贝格的菲利普，他肯定在谈判中发挥了相当重要的作用，并在不久之后接替雷纳德成为了科隆大主教。

1167年签订的是一项双边协议。罗马人放弃了他们关于公社独立的激进主张，接受了帝国的宗主权。反过来，巴巴罗萨也首次承认了元老院拥有管理罗马人民的权利，而元老院的成员将由他或是他指派的教士来任命。元老院和全体罗马人民都向皇帝宣誓效忠。他们同意保护罗马帝国的皇权不受任何人侵犯，协助皇帝进行城内城外的所有行政管理，并绝不以言语或是行动参与到任何针对皇帝的行动里去。巴巴罗萨以自己的方式确认了元老院的永久存在，他说，元老院的地位将因其作为一个帝国认可的行政管理机构而不是一个自发组成的人民议会得到提高。他承诺不会将公社从帝国那里获得的合法财产和领地权力转让给第三方——这里很可能指的是教廷。他承认了城市的一些约定俗成的传统，还有三、四代人租约的有效性。最后，他还免除了罗马人民在帝国境内的任何过路费或是其他征收费用，这一规定使得罗马的公民们收益，他们是反对罗马贵族和教廷的公社运动的中坚力量。

所有这些规定都将包含在皇帝即将发布的，由诸侯们共同署名的金玺诏书中。1167年帝国与罗马公社的这份条约沿袭了自查理曼大帝于774年颁布特权以来，帝国对教会条约的相同格式，这暗示了此时帝国眼中罗马公社其实和教会拥有相同等级地位。学者们长期以来认为，是8月2日爆发的疫情阻止了这份金玺诏书的颁布，但尤尔根·彼得森（Jürgen Petersohn（1935-2017），一位德国中世纪史学家）证明，该诏书正是1188年元老院承认教廷主权的范本。巴巴罗萨在1153年的康斯坦茨条约中接受了教宗对罗马的统治权，然后又声称教宗是从帝国那里得到了对罗马的行政权和教廷领地，他将教宗完全排除在其教区的世俗治理之外，并将罗马视为一个帝国城邦。当帝国的统治在意大利其他地方分崩离析的时候，罗马人民仍然忠于巴巴罗萨，因为他既没有对他们横征暴敛，也没有逼迫他们去效忠他扶植的对立教宗。

罗马投降的条件最迟在8月3日就已经谈妥了。罗马人交给了巴巴罗萨280名人质，皇帝在他们的行政机构了安插了50名议员。他还盘前自己的代理人，包括阿塞伯·莫雷纳，在几天内接受了个别罗马贵族的宣誓效忠，但引人注目的是，弗兰吉帕尼、皮耶罗尼（Pierleoni）和科尔西（Corsi）家族并不在其中，他们在城里拥有自己的塔楼以及要塞。罗马人承诺之后会对这些亲教廷势力的家族，特别是弗兰吉帕尼家族下手。阿塞伯·莫雷纳曾多次请求允许他返回家乡，但都没有被允许。他被迫继续工作直到开始发高烧，才被允许离开。他被人用轿子从战乱的城市一路抬到锡耶纳，在那里他待了大约12个星期，于10月18日去世。
