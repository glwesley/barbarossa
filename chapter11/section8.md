### 巴巴罗萨再战米兰（8）

#### The Defiance of Archbishop Eberhard I of Salzburg 萨尔茨堡大主教埃伯哈德一世的反抗

9月8日后不久，更加愤怒的巴巴罗萨表达了他于其他诸侯们对埃伯哈德第三次缺席的惊怒。他写道：“向皇帝提供必要的建议是教会诸侯们的特殊职责。”萨尔茨堡教会尤其有义务为帝国提供服务，因为它被执掌此地的前任大主教们赋予了比其他任何一个教区都要丰富的特殊含义——这可能是巴巴罗萨给出来的一个不太友好的暗示，他很可能会因为不遵守封建义务而剥夺埃伯哈德的地位。皇帝提醒埃伯哈德，耶稣曾在《马太福音》22:21中说过，“凯撒的归凯撒。”这一次，他命令埃伯哈德和其部队于1162年4月23日抵达维罗纳郊外的战场。埃伯哈德必须在巴巴罗萨的宫廷牧师及使者——科隆的公证人伯查德的见证下宣誓遵守誓言。巴巴罗萨警告他，如果他仍然不听话，他就会于其他诸侯还有教士们协商，采取更多的措施以保障“在不削弱上帝的荣誉与信仰的前提下，为帝国提供义务内的服务。”巴巴罗萨同时还在搜集证据威胁大主教。他向古尔克的罗曼转达了他写给埃伯哈德的信件，要求这位主教要么敦促大主教尽快遵守命令，要么他就必须和其他诸侯们一样，不得不裁定埃伯哈德犯下了“蔑视和侮辱皇帝”的罪行。

1161年12月，伯查德会见了埃伯哈德，他在一封记载了关于他代表巴巴罗萨出使事件的信中，向科隆皇家编年史的编者锡格堡的尼古拉斯修士（Abbot Nicholas of Siegburg）将其描述为“一个虔诚的主教和一个疯狂的老人。”大主教毫不掩饰自己对亚历山大三世正统地位的拥护，并对履行其应尽的军事义务含糊其辞。在随后，埃伯哈德与阿奎利亚都主教乌尔里希在卡林西亚的维拉赫举行的一次会议上（伯查德也参加了这次会议），伯查德公开宣读了一封遗失的信件，这封信是巴巴罗萨写给萨尔茨堡教会的诸侯和官员的，皇帝在信中命令他们确保大主教将服从帝国。愤怒的埃伯哈德站在一块石头上，以手划了个十字，宣称圣灵曾降下旨意给他，并发表了有利于亚历山大三世的一些言论。这篇演说的效果实在太好，以至于伯查德发现巴巴罗萨似乎很难重新获得这些听众们对自己的效忠。然而，埃伯哈德最终还是宣布他将乐意为帝国效劳，伯查德报告道，他提出向皇帝支付一笔钱，以替代他不能亲自履行的军事义务。大主教还是在试图寻找一种能够使上帝和凯撒都满意的方法。

在与伯查德接洽过后，1161年12月埃伯哈德致信巴巴罗萨，说他满怀敬意地收到了9月8日前后不久皇帝寄来的信。虽然先前的皇帝们曾给萨尔茨堡教会带来过丰厚的捐赠，但这些财富大部分已经被转为不动产，因此教会缺乏进行军事远征的物质资源。此外，他表示自己年事已高，身体虚弱，且还有修道院事务要处理。不过由于埃伯哈德希望能够得到巴巴罗萨的宽恕，所以他需要派一名特使前往商议他该付出多少金钱来弥补他不能够提供军事服务。巴巴罗萨直到伯查德回到宫廷，并当着埃伯哈德的使节详细报告了他与大主教的会晤情况后才做出了答复。伯查德在写给尼古拉斯修士的信中表示，皇帝很生气，并且很有可能真的要撤销埃伯哈德作为萨尔茨堡大主教的职务。巴巴罗萨自己倒是在1162年1月写给埃伯哈德的心中表现得还比较克制，在致辞中，埃伯哈德依然有资格接受皇帝的恩典和祝福。皇帝否决了埃伯哈德提出的经济赔偿，因为“我们并不习惯接受任何心怀怨恨之人的金钱”；但他准备接见一次大主教，与他讨论教会和帝国的关系问题。

巴巴罗萨有足够充足的理由对埃伯哈德感到恼火：他蔑视皇帝的命令、拒绝参与无论是他在战场上的军事活动还是他在宫廷中的会议；他拒绝为巴巴罗萨提供军事服务，最重要拒绝提供他作为封臣有义务提供的建议；还有他认可亚历山大三世作为合法教宗，并帮助他进行舆论上的宣传。不过我们需要注意的是，最后一点在帝国的书信往来中从未明确被指出。然而埃伯哈德最最恶劣的违法行为时，他拒绝亲自会见君主，理由是他已经被开除教籍，这对皇帝的荣誉是不可容忍的侮辱。用更通俗一些的话语来解释的话，大主教就是公然在给皇帝难堪。这才是为什么巴巴罗萨执意要与大主教相见，而不允许他仅仅是付出金钱便敷衍过关。在1162年1月写给阿德蒙特的戈弗雷修士（Abbot Godfrey of Admont）的信中，大主教将巴巴罗萨的信比作惊雷。

大主教希望班贝格主教埃伯哈德还有宫相杜尔门茨的乌尔里希（Ulrich of Dürrmenz）能够为他说几句好话——与意大利城邦不同，萨尔茨堡的埃伯哈德可以轻易依靠有权有势的朋友在宫廷里代表他行事。事实上，宫相在给萨尔茨堡的埃伯哈德的信中提到了他们之间的亲近关系（familiaritas），这个词汇可能是指他们二人之间是熟人或者亲属关系。班贝格的埃伯哈德和宫相乌尔里希于1162年1月向他报告道，他们已经成功平息了巴巴罗萨的怒火，但萨尔茨堡的埃伯哈德与布里克森的哈特曼还有古尔克的罗曼必须还得亲自来走一趟。班贝格主教向大主教保证，只要他到场，巴巴罗萨就将揭过这事，而乌尔里希则警告埃伯哈德最好不要再拿身体原因作为借口。埃伯哈德不惧任何强迫，也就是说他其实不介意面对承认亚历山大三世作为正统教宗的压力，在他与皇帝进行了一番“私下交谈”之后，他便被同意可以自由离开了。随后大主教写信给杜尔门茨的戈弗雷修士，告诉他皇帝已经准许他们离开。

萨尔茨堡的埃伯哈德在哈特曼、罗曼和赖谢斯贝格修道院的教务长格霍赫（Provost Gerhoch of Reichersberg）的陪同下，在前往巴巴罗萨宫廷的路上拒绝与维克多四世在克雷莫纳会面。对立教宗随后派了自己的两名枢机主教先于埃伯哈德去面见巴巴罗萨，试图激起皇帝对这位老修士的愤怒，但并没有成功。大主教在3月米兰陷落后抵达帕维亚，受到了巴巴罗萨的欢迎。维克多四世的两位枢机主教、12位主教还有其他许多诸侯都参与了埃伯哈德与皇帝之间并不那么私密的谈话。大主教在受到尖锐质疑时仍坚持自己效忠亚历山大三世的立场，并且只是匆匆与枢机主教随便敷衍了几句话，但巴巴罗萨对此并没有表示任何反对意见。因为对于他而言，只要埃伯哈德愿意到场出席自己的集会，就足以弥补他被轻视的荣誉了。大主教在宫廷里待了很久，还参加了巴巴罗萨在4月8日举行的复活节庆典。而皇帝为了显示他对大主教的青睐，还为赖谢斯贝格和古尔克都颁发了特许状（古尔克在12世纪基督教的法律地位是独一无二的）。

在他余下的岁月里，埃伯哈德成功平衡了他的立场与忠诚之间的问题。巴巴罗萨没有邀请大主教参加位于圣让·德·洛斯内的会议，在那里他希望路易七世能够承认维克多四世的合法地位；1163年2月28日，亚历山大三世任命埃伯哈德作为教宗在德意志王国的代表，因为他是当时德意志地区6位大主教中唯一一个承认他合法地位的人。尽管他有着支持亚历山大三世的立场，但1163年3月底，巴巴罗萨还是在美因茨的宫廷里体面地接待了大主教和哈特曼，并允许两人安全离开。作为教宗的使节，大主教可能是希望教会分裂能朝着对亚历山大三世有利的方向解决。巴巴罗萨尽力忽略大主教在教会分裂中的立场，于1164年春要求埃伯哈德在哈特曼与罗曼，甚至还可能包括巴巴罗萨的堂兄施蒂利亚的奥托卡三世的协助下，尽力协调皇帝的两位巴本堡家族的叔父，奥地利的亨利与帕绍的康拉德之间的争端。早些时候，皇帝为了回应维罗纳大游行（March of Verona），曾在1164年5月31日的五旬节（Pentecost）召埃伯哈德“带尽可能多的人马”来到特雷维索（Treviso），因为皇帝既需要他提供建议，也需要帮助平定帕多瓦（Padua）和维琴察（Vicenza）这两个城市的叛乱。我们不知道巴巴罗萨为何会期望埃伯哈德同时出现在奥地利和威尼斯，也不知道这位大主教这次会如何回应皇帝对他提出的提供军事援助的要求，但1163年6月22日，他的去世使他彻底摆脱了这一切困扰他的问题。

我们只能猜测，为什么同样是有过对皇帝不敬的侮辱行为，皇帝对待萨尔茨堡的埃伯哈德便与对待其他意大利城邦的态度不同；也许一个很重要的原因是，埃伯哈德的继任者大主教康拉德二世和阿达尔贝特二世分别是巴巴罗萨的叔叔和堂兄。当然，他们与皇帝的亲戚关系远比埃伯哈德在宫廷中那些熟人的关系要紧密。也许巴巴罗萨的确非常尊重埃伯哈德，亚历山大三世在任命他为德意志地区的教宗代表时，也特别提到了他的“虔诚、博学和谦卑”。而且也有可能皇帝认为和一个年长的教会诸侯动武并没有什么好处，毕竟这个老人家很可能没多久自己就会先去世，而且如果他认为现在维克多四世受到普遍承认已经是板上钉钉的大势所趋的话，他其实不介意对自己的反对者表现得宽宏大量一点。然而1162年9月，皇帝在圣让·德·洛斯内的外交失败使得他不得不采取更加严厉的手段来对付自己的敌人，他徒劳地想要强行让德意志地区承认他所支持的对立教宗，甚至最终还烧毁了萨尔茨堡。
