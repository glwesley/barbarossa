# Summary

* [摘要](README.md)

* [第一章 两个名门望族](chapter01/README.md)

    * [霍亨斯陶芬家族的起源(1)](chapter01/section1.md)

    * [霍亨斯陶芬家族的起源(2)](chapter01/section2.md)

    * [霍亨斯陶芬家族的起源(3)](chapter01/section3.md)

    * [霍亨斯陶芬家族的起源(4)](chapter01/section4.md)

    * [霍亨斯陶芬家族的起源(5)](chapter01/section5.md)

* [第三章 幸运的年轻人](chapter03/README.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(1)](chapter03/section1.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(2)](chapter03/section2.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(3)](chapter03/section3.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(4)](chapter03/section4.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(5)](chapter03/section5.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(6)](chapter03/section6.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(7)](chapter03/section7.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(8)](chapter03/section8.md)

    * [巴巴罗萨与霍亨斯陶芬家族的权力之路(9)](chapter03/section9.md)

* [第四章 罗马人的国王](chapter04/README.md)

    * [罗马人之王巴巴罗萨(1)](chapter04/section1.md)

    * [罗马人之王巴巴罗萨(2)](chapter04/section2.md)

    * [罗马人之王巴巴罗萨(3)](chapter04/section3.md)

    * [罗马人之王巴巴罗萨(4)](chapter04/section4.md)

    * [罗马人之王巴巴罗萨(5)](chapter04/section5.md)

    * [罗马人之王巴巴罗萨(6)](chapter04/section6.md)

    * [罗马人之王巴巴罗萨(7)](chapter04/section7.md)

    * [罗马人之王巴巴罗萨(8)](chapter04/section8.md)

    * [罗马人之王巴巴罗萨(9)](chapter04/section9.md)

* [第五章 流动的皇权](chapter05/README.md)

    * [巴巴罗萨环游神圣罗马帝国(1)](chapter05/section1.md)

    * [巴巴罗萨环游神圣罗马帝国(2)](chapter05/section2.md)

    * [巴巴罗萨环游神圣罗马帝国(3)](chapter05/section3.md)

    * [巴巴罗萨环游神圣罗马帝国(4)](chapter05/section4.md)

    * [巴巴罗萨环游神圣罗马帝国(5)](chapter05/section5.md)

    * [巴巴罗萨环游神圣罗马帝国(6)](chapter05/section6.md)

    * [巴巴罗萨环游神圣罗马帝国(7)](chapter05/section7.md)

* [第六章 通向罗马之路](chapter06/README.md)

    * [通向罗马之路(1)](chapter06/section1.md)

    * [通向罗马之路(2)](chapter06/section2.md)

    * [通向罗马之路(3)](chapter06/section3.md)

    * [通向罗马之路(4)](chapter06/section4.md)

    * [通向罗马之路(5)](chapter06/section5.md)

    * [通向罗马之路(6)](chapter06/section6.md)

    * [通向罗马之路(7)](chapter06/section7.md)

    * [通向罗马之路(8)](chapter06/section8.md)

    * [通向罗马之路(9)](chapter06/section9.md)

    * [通向罗马之路(10)](chapter06/section10.md)

    * [通向罗马之路(11)](chapter06/section11.md)

    * [通向罗马之路(12)](chapter06/section12.md)

    * [通向罗马之路(13)](chapter06/section13.md)

    * [通向罗马之路(14)](chapter06/section14.md)

* [第七章 新的安排](chapter07/README.md)

    * [巴巴罗萨与帝国内政(1)](chapter07/section1.md)

    * [巴巴罗萨与帝国内政(2)](chapter07/section2.md)

    * [巴巴罗萨与帝国内政(3)](chapter07/section3.md)

    * [巴巴罗萨与帝国内政(4)](chapter07/section4.md)

    * [巴巴罗萨与帝国内政(5)](chapter07/section5.md)

    * [巴巴罗萨与帝国内政(6)](chapter07/section6.md)

    * [巴巴罗萨与帝国内政(7)](chapter07/section7.md)

    * [巴巴罗萨与帝国内政(8)](chapter07/section8.md)

    * [巴巴罗萨与帝国内政(9)](chapter07/section9.md)

    * [巴巴罗萨与帝国内政(10)](chapter07/section10.md)

    * [巴巴罗萨与帝国内政(11)](chapter07/section11.md)

    * [巴巴罗萨与帝国内政(12)](chapter07/section12.md)

* [第八章 神圣的帝国](chapter08/README.md)

    * [神圣的帝国(1)](chapter08/section1.md)

    * [神圣的帝国(2)](chapter08/section2.md)

    * [神圣的帝国(3)](chapter08/section3.md)

    * [神圣的帝国(4)](chapter08/section4.md)

    * [神圣的帝国(5)](chapter08/section5.md)

    * [神圣的帝国(6)](chapter08/section6.md)

    * [神圣的帝国(7)](chapter08/section7.md)

* [第九章 第二次意大利战役](chapter09/README.md)

    * [第二次意大利战役(1)](chapter09/section1.md)

    * [第二次意大利战役(2)](chapter09/section2.md)

    * [第二次意大利战役(3)](chapter09/section3.md)

    * [第二次意大利战役(4)](chapter09/section4.md)

    * [第二次意大利战役(5)](chapter09/section5.md)

    * [第二次意大利战役(6)](chapter09/section6.md)

    * [第二次意大利战役(7)](chapter09/section7.md)

    * [第二次意大利战役(8)](chapter09/section8.md)

* [第十章 教会的分裂](chapter10/README.md)

    * [教会分裂(1)](chapter10/section1.md)

    * [教会分裂(2)](chapter10/section2.md)

    * [教会分裂(3)](chapter10/section3.md)

    * [教会分裂(4)](chapter10/section4.md)

    * [教会分裂(5)](chapter10/section5.md)

    * [教会分裂(6)](chapter10/section6.md)

    * [教会分裂(7)](chapter10/section7.md)

* [第十一章 米兰的失败](chapter11/README.md)

    * [米兰的失败(1)](chapter11/section1.md)

    * [米兰的失败(2)](chapter11/section2.md)

    * [米兰的失败(3)](chapter11/section3.md)

    * [米兰的失败(4)](chapter11/section4.md)

    * [米兰的失败(5)](chapter11/section5.md)

    * [米兰的失败(6)](chapter11/section6.md)

    * [米兰的失败(7)](chapter11/section7.md)

    * [米兰的失败(8)](chapter11/section8.md)

    * [米兰的失败(9)](chapter11/section9.md)

* [第十二章 追捕亚历山大三世](chapter12/README.md)

    * [追捕亚历山大三世(1)](chapter12/section1.md)

    * [追捕亚历山大三世(2)](chapter12/section2.md)

    * [追捕亚历山大三世(3)](chapter12/section3.md)

    * [追捕亚历山大三世(4)](chapter12/section4.md)

    * [追捕亚历山大三世(5)](chapter12/section5.md)

    * [追捕亚历山大三世(6)](chapter12/section6.md)

    * [追捕亚历山大三世(7)](chapter12/section7.md)

    * [追捕亚历山大三世(8)](chapter12/section8.md)

    * [追捕亚历山大三世(9)](chapter12/section9.md)

    * [追捕亚历山大三世(10)](chapter12/section10.md)

    * [追捕亚历山大三世(11)](chapter12/section11.md)

    * [追捕亚历山大三世(12)](chapter12/section12.md)

    * [追捕亚历山大三世(13)](chapter12/section13.md)

    * [追捕亚历山大三世(14)](chapter12/section14.md)

    * [追捕亚历山大三世(15)](chapter12/section15.md)

    * [追捕亚历山大三世(16)](chapter12/section16.md)

* [第十三章 苦涩的回归](chapter13/README.md)

    * [巴巴罗萨的归来(1)](chapter13/section1.md)

    * [巴巴罗萨的归来(2)](chapter13/section2.md)

    * [巴巴罗萨的归来(3)](chapter13/section3.md)

    * [巴巴罗萨的归来(4)](chapter13/section4.md)

    * [巴巴罗萨的归来(5)](chapter13/section5.md)

    * [巴巴罗萨的归来(6)](chapter13/section6.md)

    * [巴巴罗萨的归来(7)](chapter13/section7.md)

    * [巴巴罗萨的归来(8)](chapter13/section8.md)

    * [巴巴罗萨的归来(9)](chapter13/section9.md)

    * [巴巴罗萨的归来(10)](chapter13/section10.md)
