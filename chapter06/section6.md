### 通向罗马之路（6）

#### Lombardy 伦巴第

长期以来，人们一直认为德意志地区的主教们在王室的军事行动中十分活跃，尤其是在巴巴罗萨统治期间；在12世纪，德意志主教以他们好战的倾向而闻名。1177年在威尼斯和1184年在美因茨，其他世俗诸侯和教会诸侯率领的军队人数比值大约是1:4.5和1:2.5——假设这些人都是身体健全的骑士。除了巴巴罗萨在1172年第二次入侵波兰的战争之外，他每次出征时，随行的教会诸侯都要多于世俗诸侯。不过这些数字是具有欺骗性的，因为在德意志地区有44位主教是王室的直属封臣，而在1180年之前只有18位世俗诸侯是王室的直属封臣。如果考虑到了这一数量上的差距，那么主教们的财产占诸侯们总数的70%，占参与了巴巴罗萨战争的诸侯们的67%。与世俗诸侯们的情况一样，一些主教——尤其是美因茨大主教、科隆大主教、沃尔姆斯主教和维尔茨堡主教比其他主教更多地参与了军事方面的事务。除了达塞尔的雷纳德（Rainald of Dassel）和布赫的克里斯蒂安（Christian of Buch）是明显的例外，大多数主教都不会亲自领兵作战。

1154年12月5日，当巴巴罗萨还在隆卡利亚的时候，他应意大利地区的主教们还有诸侯们的需求，续订了洛塔尔三世在1136年颁布的宪法，该宪法禁止贵族在没有其领主的许可下出售或抵押自己的封地，无论是部分还是全部。与他的前任不同，巴巴罗萨赋予了这一禁令追溯效力。为了阻止领地的卖方通过一些欺诈手段来隐瞒这种交易，法院裁定，一经发现，涉及交易的领地将转给卖方的上级领主，而起草买卖相关法律文书的公证人将被免职并剁去一只手【草，这就有点狠了】。此外，德意志和意大利地区的诸侯如果不听从其领主的召唤参加巴巴罗萨前往罗马加冕的行动，将被剥夺领地。鉴于没有任何证据表明，在1154年之前，德意志地区的诸侯作为国王的封臣在法律上有为国王提供这种兵役的义务【指跟随巴巴罗萨前往罗马巡游并加冕】，因此，将意大利地区的这种法律管理推广到德意志地区就像《沃尔姆斯条约》一样，是以封建关系规定君主与诸侯关系的又一重要举措。

仅仅是依靠习惯法是无法阻止领土的买卖与商业化，四年后巴巴罗萨在隆卡利亚召开的另一次集会裁定，凡是在不知情的情况下购买了被非法转让的封地的买房，可以起诉卖方并要求退款。不过这条规定并没有说，如果卖方无法退钱的话，买房可以理所当然地保留这块土地。此外，在意大利和德意志的诸侯现在被要求有义务参加国王召唤他们进行的任何战役，而不仅仅是陪同巴巴罗萨前往罗马巡游并加冕。他们可以派一个合适的代理人，或者缴纳其封地年收入的一般来替代这种义务，可以看作是英国人所说的scutage或者shield money，而不遵守规定的惩罚则是被剥夺封地。巴巴罗萨是想确保此后的军事行动都能有充足兵力。不过鉴于诸侯们拒绝参加巴巴罗萨之后的战役，以及1220年左右写成的《The Saxon Mirror》中也提到，诸侯们跟随国王前往意大利的官方义务仍然仅限于去罗马巡游并加冕。归根结底，陪同国王去罗马加冕或者参加任何战役不仅仅只是一种法律义务，在这样一个充满骑士精神的社会中，这直接关系到诸侯们的地位和荣誉，也是赢得名声和获得皇帝青睐的一种方式。为国王服务既能获得物质上的回报，也能得到精神和地位上这种抽象的回报。为了拉拢更多的人忠于自己，巴巴罗萨不得不同时迎合他们这两方面的需求。

毫无疑问，此时热那亚的使节急于确保巴巴罗萨与他们支架年的友好关系，在1154年的隆卡利亚集会上他们向他赠送了狮子、鸵鸟、鹦鹉和其他他们在第二次十字军期间夺取阿尔梅里亚（Almeria）和里斯本（此处应为托尔托萨而不是里斯本）时获得的战利品。阿斯蒂主教（Bishop of Asti）和蒙费拉托侯爵威廉五世——据他的内兄弟弗赖辛的奥托所言，“可以说是意大利唯一一个能够不受制于城邦权威的封建贵族”，抱怨了阿斯蒂和基耶里（Chieri）市民的无礼。几周后，这两座城市被巴巴罗萨以及蒙费拉托侯爵威廉摧毁。威廉有可能以及于1152年10月通过他的姐夫——比安德拉特伯爵吉多（Count Guido of Biandrate）向国王表达了他的抗议，当时这位伯爵正好要将洛迪的金碗呈送给巴巴罗萨。

奥托对1153年3月在康斯坦茨发生的事情只字未提【指洛迪对巴巴罗萨提交关于米兰的诉讼】，他接着开始讲述巴巴罗萨在隆卡利亚地区与米兰的对抗。他说，科莫（Como）和洛迪的执政官当着米兰的两位执政官翁贝托·德·奥尔托（Oberto de Orto）和格拉尔多·内格里（Gerardo Negri）的面，“对米兰人民的傲慢态度表示遗憾与悲哀”。奥托没有说明他的侄子巴巴罗萨将对此做出怎样的回应。相反，她说巴巴罗萨此时正计划造访波河河谷上游，他要求两位执政官引导他通过米兰人的领地，并为他安排适当的扎营地点。翁贝托是第一部伦巴第封建法律汇编《Libri feudorum》的作者，有人声称他是1154年巴巴罗萨在隆卡利亚的首席法律顾问，他可能参与起草了巴巴罗萨第一部关于封地（fiefs）的法律。我们很难认为翁贝托为巴巴罗萨提供服务与巴巴罗萨和米兰之间的敌对关系这两件事情不矛盾，毕竟一年前米兰人当众销毁了他的印章。

奥托·莫雷纳承认，在隆卡利亚其实米兰以及与巴巴罗萨结盟，但法官补充道米兰人其实并非出于真心，他们还向国王开出了4000马克——即唱过900公斤银子的空头支票。而巴巴罗萨也意识到了这一点。他在1157年给他的叔叔奥托的信件中写道：“骄傲而狡猾的米兰人发了虚假的誓言，”并向他“许诺了很多钱，希望通过我们的赠予，让他们可以成为科莫和洛迪的上级领主。”巴巴罗萨坚持认为，无论是恳求还是金钱都不可能打动他，他大概是对先前的事情太耿耿于怀了。看起来国王在隆卡利亚似乎一直致力于推行解决伦巴第地区冲突的政策，这样他就可以毫无负担地前往罗马，而不必担心后方会爆发冲突。一位来自米兰的无名氏在1177年后不久撰写了巴巴罗萨压迫伦巴第的历史，他记载道，巴巴罗萨在隆卡利亚命令米兰人和帕维亚人停止双方之间的争斗，并把他们在去年夏天交战中抓获的俘虏都交给他，作为保证他们今后和平的人质。事后看来，这位无名氏指责巴巴罗萨从一开始就只是想让伦巴第人臣服于他的权威，并且非常狡猾地希望通过支持较弱的帕维亚来对抗较强的米兰来达成这一目的；但实际上并没有任何理由可以认为1154年的巴巴罗萨会有这种动机。

事实上巴巴罗萨与米兰的关系是在1157年两位执政官为他的军队带错路之后才恶化的，他坚持认为米兰人是故意的，而米兰人也拒绝了他希望重建科莫和洛迪的想法。巴巴罗萨最初有可能是想从米兰东南的隆卡利亚前往米兰东北10英里处的蒙扎加冕为意大利国王，就像康拉德在1128年所做的那样，当然这一愿望也和歌颂他事迹的作者所描述的想法一致。当然，如果他在这趟旅程里没有访问伦巴第最重要的城市米兰就会显得很突兀。帝国派和米兰派的编年史学家对于米兰的议会究竟是如何接待了巴巴罗萨以及他的军队持有不同的看法，但他们却一致表示国王对于米兰对他的冒犯行为暴跳如雷。弗赖辛的奥托记载，米兰的执政官带领巴巴罗萨穿越了一片“荒地”，使得军队无法获得补给，而且途中巴巴罗萨还遭遇了一场暴雨，让他非常生气。接着米兰拒绝重建科莫和洛迪两座城市以及还试图贿赂他使得他更加恼火，于是将两位执政官解雇。奥托·莫雷纳表示，军队因为缺乏马匹的饲料不得不前往洛迪以西的兰德里亚诺（Landriano），并在那里修整了三天（1154年12月7日至9日）。而来自米兰的无名氏则讲述了一个截然相反的故事。他说巴巴罗萨在兰德里亚诺释放了他在隆卡利亚接受的帕维亚人质，但却把米兰人俘虏绑在马尾上在泥地里拖行。有些人设法逃跑，而有些人则通过贿赂得到了自由。负责为军队提供补给的面包师以及其他供应商被洗劫一空之后赶走。事实上米兰人没理由刻意与帝国军队交恶，他们更可能希望并催促德意志人赶紧开拔。最合理的解释可能是，米兰的确试图努力为帝国军队提供补给，却并没有完成这一任务——很可能是因为米兰与帕维亚之间的战争极大地破坏了附近的乡村。然而巴巴罗萨却将此视为米兰人对自己的一种背叛，并对此予以报复。
