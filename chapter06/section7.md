### 通向罗马之路（7）

#### Lombardy 伦巴第

不管是亲帝国还是反帝国的资料都对接下来发生的事情持认可态度。米兰人在米兰西南10英里的罗萨泰（Rosate）的城镇和城堡里驻扎了500名骑士。巴巴罗萨要求守军——包括妇女和儿童离开，然后他们照做了。帝国军队在罗萨泰劫掠了物资之后将其焚毁。国王的一些骑士进攻到米兰城前，打伤或俘虏了许多米兰人。尽管国王做出了这些挑衅举动，米兰人还是表现出了非凡的忍耐力，试图避免与国王彻底决裂。据弗赖辛的奥托的记载，为了平息巴巴罗萨的怒火，米兰人下令摧毁了执政官格拉尔多·内格里的宅邸——因为他被指控误导了帝国军队；但这一赎罪式的行为显然并没有达到目的，因为巴巴罗萨既没有亲自参与判决对这位执政官的惩罚，也没有亲自参与执行这一惩罚。尽管米兰人悲愤交加，但敌对阵营的奥托·莫雷纳也不得不承认，他们在公众场合表现得异常冷静承受了这一损失。几天之后，当巴巴罗萨驻扎在比亚恩德拉泰的时候，米兰人提出要履行之前的承诺付给他4000马克。但国王怒斥他们是一群邪恶、狡诈的骗子，并将他们逐出宫廷，在冲突升级时他还要求米兰无条件满足他关于恢复科莫和洛迪的要求。

弗赖辛的奥托毫不掩饰地表示，巴巴罗萨接下来采取的措施是对帕维亚、诺瓦拉（Novara）以及比亚恩德拉泰伯爵吉多有利的。1154年12月15日，巴巴罗萨的军队越过米兰以西的波河支流提契诺河（Ticino），摧毁了米兰为了进攻承认吉多伯爵宗主地位的诺瓦拉和帕维亚而修建的两座木桥。在这两座城市的军队的协助下，国王在圣诞节前摧毁了位于诺瓦拉东北部的莫莫（Momo）、加利亚泰（Galliate）以及特雷卡泰（Trecate）的城堡，据奥托说，这些城堡是米兰为了控制诺瓦拉的人民而修建的。巴巴罗萨在1157年告诉他的叔叔，他在这些米兰人的建筑被摧毁后度过了一个非常愉快的圣诞节。他还可能在1154年底对米兰的非法行为做出了初步的判决。也许在巴巴罗萨看来，让他最感到被羞辱的地方在于，他其实缺乏对米兰采取更多实质性惩罚的力量。

巴巴罗萨继续向西，途径韦尔切利（Vercelli）来到都灵。在那里他越过波河到达都灵南部的基耶里。威廉侯爵在隆卡利亚集会上坚持要惩罚基耶里的市民，于是他们在巴巴罗萨接近时便逃离了；巴巴罗萨在那里停驻了几天，搜刮了那里丰富的补给物资，之后摧毁了这座城市的防御工事并将其焚毁。阿斯蒂在威廉侯爵的手中遭遇了类似的命令。在离开阿斯蒂之前，巴巴罗萨颁布了一项法令以约束他手下的行为。他要求每个人，无论级别高低都要发誓不得再营地的范围内佩剑。违反者将被剁去一只手，甚至直接斩首。【你们中世纪德意志时真的野蛮啊，动不动就砍头砍手，然后到哪儿都劫掠一空再夷为平地，你这是神罗皇帝还是阿勒曼尼大酋长啊？】奥托带着些许虔诚的语气评论道：“在这一必要且明智的命令下达后，年轻人不经过大脑思考的暴力行径便得到了平息。”巴巴罗萨大概是在前往罗马加冕的路上意识到，他需要为自己的军队准备足够的补给，并努力维持他们的军纪。【我没觉得你意识到了好吗，不是你动不动就把别人城市洗劫一空再烧个精光吗……】

The Siege and Destruction of Tortona 对托尔托纳的围攻及破坏

激起伦巴第地区对巴巴罗萨反抗情绪的高潮事件，是他对托尔托纳进行了长达两个月的围攻和破坏（1155年2月13日至4月20日），托尔托纳位于帕维亚西南边，处在连接米兰和热那亚的道路上。再一次地，有与帝国一方消息来源——即弗赖辛的奥托与奥托·莫雷纳截然相反的消息来源，即托尔托纳的一位修士，他作为目击者描述了他的城市是如何被摧毁的，而米兰的那位无名氏很可能是以这位修士的叙述作为其消息来源。他们共同表达的潜台词都是巴巴罗萨的兵力不足，这使得他很难攻下任何一座小城市，更不用提那些公认的及其坚固的城市，所以他不得不依赖他在意大利的盟友帕维亚以及蒙费拉托的威廉。资料来源差异最大的地方是关于托尔托纳的投降以及被摧毁的情况。

托尔托纳的教士坚持认为，托尔托纳主教奥伯特和执政官在巴巴罗萨到达意大利之前就已经欢迎并接待过巴巴罗萨的使节，还向国王宣誓效忠并支付了fodrum。他们勉强答应了巴巴罗萨在隆卡利亚提出的要求，交出了被他们俘虏的帕维亚人，但他们拒绝再向巴巴罗萨支付一笔巨款，因为这笔钱会毁了他们。根据奥托·莫雷纳的说法，托尔托纳人之所以无视巴巴罗萨的传唤，也对帕维亚在巴巴罗萨面前对他们的指控置若罔闻，是因为他们认为国王对他们抱有偏见，更有可能是因为他们是米兰那一派的。弗赖辛的奥托表示，帕维亚认为托尔托纳是比米兰更危险的敌人，因为托尔托纳对帕维亚在波河以南的领地构成威胁，而巴巴罗萨之所以围攻托尔托纳，也是因为它是米兰的盟友。

巴巴罗萨从阿斯蒂来到托尔托纳西南部的博斯科（Bosco），2月7日，他提前派他年仅十几岁的弟弟康拉德、扎林根的贝特霍尔德四世公爵还有他的副官维特尔斯巴赫的奥托五世前去侦查。托尔托纳的修士表示，他们还对托尔托纳发动了一次突袭，但被击退。2月13日，他开始对托尔托纳的围攻。

托尔托纳位于亚平宁山脉脚下。城堡在山顶俯瞰着波河河谷平原，两侧几乎都是陡峭的山体，由巨大的城墙和塔楼防御，其中一座名叫Rubea或者Red的塔楼，据说是由罗马王政时代的最后一位君主骄傲者塔奎尼乌斯（Tarquin the Proud，即卢基乌斯·塔奎尼乌斯·苏培布斯（Lucius Tarquinius Superbus） 建造的。要塞只有一侧可以攀登。帕维亚的民兵和威廉侯爵增援了围攻托尔托纳的帝国军队。米兰军队大约100名骑士和200名弓箭手在他们的执政官指挥下盘踞在城堡里，和他们一起的还有担任预备队总指挥的奥比佐·马拉斯皮纳侯爵（Margrave Obizzo Malaspina）及其部下。（奥比佐出现在托尔托纳，证明意大利的大贵族们也并非从根本上反对城邦公社，他们的立场就和这些城市一样，随着自己的利益而随时可以变化。到了1157年6月，奥比佐又与蒙费拉托的威廉以及帕维亚并肩作战来对抗米兰了。）

狮子亨利和帕维亚人于2月17日发动猛攻并焚毁了卫城，而居民们则在谒见撤退到主城堡。巴巴罗萨在1157年给他的叔叔奥托写信表示，夜幕降临和一场突如其来的暴风雨使得帝国军队无法在当天一鼓作气占领城堡。巴巴罗萨制造了攻城用的工具【我猜是投石车之类的……】并对要塞进行轰击。试图破坏Rubea城堡的行动被守军挫败。一个马夫迎着守军掷出的长矛与石块爬上了塔前的垛墙，用他的个人勇气为军队树立了榜样。巴巴罗萨希望通过册封他为骑士来表彰他，但这位马夫并不想接受这种过于僭越的封赏，所以国王给予了他更多物质上的奖励。守军英勇地抵抗着，而巴巴罗萨下令将俘虏全都绞死。之后他对守军唯一的水源发动了一次突袭，命令用人和野兽的尸体将泉水的源头填住，之后还向里面投放燃烧的硫磺和沥青，使得泉水无法饮用。最后迫使托尔托纳不得不因为缺水而投降。

巴巴罗萨批准托尔托纳可以从星期四休战到复活节星期一（即3月24日至28日）。受难节当天，托尔托纳的神职人员没有身着忏悔服，而是穿着教会的法衣，拿着十字架和其他仪式器皿向着国王的营帐走来。巴巴罗萨拒绝接待这些他认为背叛了自己的人，而是派主教和一些其他受过教育的人去确认这些人来访的目的。值得一提的是弗赖辛的奥托当时并不在场，但他却在后来的长篇论述中记载了这件事。我们不能认为是奥托编造了那些教士们的讲话，因为那位没有透露姓名的托尔托纳教士很可能也是代表团中的一员，他的报告里也提到，神职人员们试图与国王交谈，但是被拒绝了。最主要的区别是，在奥托的版本中，这些神职人员只是为他们自己而辩护，他们认为自己是无辜的旁观者，不幸被困在叛乱的城市之中。而相比之下，托尔托纳教士在事件发生之后表示，这些教士更有可能是应市民们的要求前去帮他们向国王求情。​​​​
