### 教会的分裂（2）

大约在1159年4月16日，当巴巴罗萨宣布米兰人为叛乱者时，枢机主教亨利和克雷马的吉多来到了博洛尼亚附近的帝国营地。皇帝对教廷使节的接待表明，阿德里安一定是听从了埃伯哈德的建议，以更加和解的方式给君主写信，但他们具体讨论的内容我们无从得知。

班贝格大主教为大主教总结了教宗的“严厉要求”。“在没有让教宗事先知晓的情况下，皇帝不得向罗马派遣使节；罗马城的所有行政官员都只受圣彼得的管辖；只有在皇帝加冕时，才能从教宗的领地上征收fodrum；皇帝只能要求意大利的主教们宣誓效忠，但不得要求他们致敬；帝国的使节不得住在主教府里。”作为教宗想要收回教廷失去的财产和权利的计划的一部分，阿德里安还要求归还大量的地产，其清单包括蒂沃利（Tivoli）、费拉拉（Ferrara）以及罗马与位于其以北约90英里的阿夸彭登泰（Acquapendente）之间的所有土地。此外，教宗还要求获得巴巴罗萨在1152年授予韦尔夫六世的领地，包括玛蒂尔达的遗产、斯波莱托公国、撒丁岛和科西嘉岛。简而言之，教宗禁止皇帝在罗马行使任何世俗的管辖权，并要求获得第勒尼安海（Tyrrhenian Sea）上岛屿的领主权。

如果我们相信拉赫温的另一篇说辞的话，那么巴巴罗萨在回答枢机主教们提出的要求时嘲笑了教宗。但这些话很可能是拉赫温的发明，因为皇帝不太可能引用格拉提安（Gratian）的《Decretum》或是查士丁尼的《Digest》。【很好，胡子王你的文盲又被cue了一遍。】然而，他们几乎可以肯定地表明了皇室在隆卡利亚所阐明的立场，即包括教宗在内的领主权利都是由帝国授权的。巴巴罗萨表示，如果意大利地区的主教不愿意持有他授予的封建权利的话，他倒是可以接受放弃他们的致敬。他也同意帝国使节不再在主教府中逗留，当然前提条件是这些建筑必须真的位于主教们自己的领地上；如果这些建筑是建在帝国的土地上，那么这些宫殿就属于帝国，使节们就理应可以使用这些宫殿。教宗对罗马专属主权的主张需要“更认真，更成熟的考虑”，因为它挑战了帝国尊严的本质。据称巴巴罗萨曾说：“因为根据神的旨意，我是罗马的皇帝，而这样的称呼意味着，如果罗马城的权利从我手中被夺走，那我的统治就徒有其表，拥有一个空洞的虚衔没有任何意义。”拉赫温的话在这个例子中可能呼吁了巴巴罗萨自己作为一个务实政治家的想法。然而，反过来说，如果圣彼得的权利都是帝国的附属品，那么皇帝就有权拒绝向教廷授权，并有权干预在一个受帝国管辖的城市举行的有争议的教宗选举。双方都可以推断出对方立场中的深层含义。

班贝格的埃伯哈德告知大主教，巴巴罗萨用自己的要求来反驳限制帝国的摄政权和教宗权利——就和几年后英格兰的亨利二世在与托马斯·贝克特的争端中所做的那样。皇帝反对枢机主教在未经他允许的情况下造访其他地区，住在主教宫里，在不归教廷权限管辖的案件中审理下级法院的上诉，给帝国教会造成负担，还有许多其他的事情为了版面的整洁，埃伯哈德便没有逐一列举。和在贝桑松的情况一样，巴巴罗萨试图通过指责教宗权利过于集中来煽动主教们对这一现象的不满，继而获得他们的支持。

巴巴罗萨初步提议，“不管是按照世俗法还是神权法”，将有争议的问题都提交审判和仲裁；如果这样的程序“过于严厉”，则允许“王公贵族和宗教人士出于对上帝和教会的敬爱”，设计一个和平的解决方案。然而，格里高利改革派曾坚持认为教宗不应受世俗权力的约束，所以枢机主教们断然拒绝了这第一个提议。他们决定在阿德里安的批准下，让仲裁者——即六位枢机主教代表教宗，而六位“虔诚、睿智、敬畏上帝”的帝国教会主教来代表皇帝。巴巴罗萨在自己的信中要求作为德意志等级制度中最受尊敬的成员之一的大主教埃伯哈德如果被传唤，就立刻赶来，大概是希望他能够作为自己一方的六位主教仲裁人之一。仲裁与教宗的地位并不冲突，事实上，在1177年最终达成的《威尼斯条约》的谈判中就采用了仲裁；但阿德里安坚持认为《康斯坦茨条约》才是和平的唯一基础【我寻思你自己好像也没遵守】，在他的解读中，该条约了教宗在罗马占主导地位的权力，所以他拒绝了这个提议。教廷选择和帝国全面对抗。

1159年6月，当巴巴罗萨与枢机主教奥克塔维安还有威廉谈判时，一个“渴望和平”的罗马公民代表团出现在了宫廷。巴巴罗萨于1155年6月18日举行的加冕礼以一场流血事件而告终，而教宗也被迫逃离了罗马。阿德里安于1156年11月签订《贝内文托条约》之后，在诺曼人支持下又重新回到了罗马。比起德意志的巴巴罗萨，西西里的威廉一世对罗马公社独立的威胁更为直接，包括枢机主教奥克塔维安的侄子奥托在内的几位罗马元老和贵族已经在1158年5月与达塞尔的雷纳德和维特尔斯巴赫的奥托进行了一些有些暧昧的谈判。在罗马教廷管辖下的一个总督彼得·迪·维科（Peter Di Vico，枢机主教的另一个侄子）的领导下，一队罗马特遣队和元老院的支持者在当时便已经参加过1158年8月巴巴罗萨对米兰的围攻。他们在米兰城外的加入表明了他们想要与皇帝和解的愿望，也表明了公社与罗马贵族们的和解，因为公社曾在12世纪40年代将贵族驱逐出城。巴巴罗萨在1159年6月友好地接待了罗马人——准确地说，拉赫温补充道，“因为他在先前的远征中对他们并不很友好”，巴巴罗萨向大主教埃伯哈德表示，这些罗马人对教宗的要求“感到惊讶和愤慨。”在他们辞别的时候，巴巴罗萨赐予了他们礼物。巴巴罗萨对罗马使节的友好接待，是对阿德里安要求皇帝遵守康斯坦茨条约的公开拒绝，该条约显然禁止这种私自的单独谈判。

在枢机主教奥克塔维安于1159年6月在宫廷逗留的这段期间，皇帝将罗马东北的特尔尼（Terni）郡赏赐给了他和他的兄弟——蒙蒂切利伯爵（Monticelli），他“最敬爱的封臣和朋友”与远亲，并包含了郡内相关的摄政权，但归属帝国的管辖权和一些荣誉头衔除外。奥克塔维安在枢机主教团中的支持比在罗马城更有价值。大约是在同一时间，巴巴罗萨将圣彼得大主教及其财产置于帝国的管辖下，并给予他们一百年的期限来收回被转让的财产和权利。一些教士后来成为了对立教宗维克多四世的热心支持者，尽管他们在法律上对选举新教宗所起的作用实在是微乎其微。

在阿德里安拒绝了巴巴罗萨的仲裁提议后，巴巴罗萨应枢机主教的要求，派维特尔斯巴赫的奥托、比亚恩德拉泰伯爵吉多和亚琛的教务长赫里伯特——他后来成为了贝桑松大主教，前往罗马与阿德里安议和，如果议和失败，他们就将转而与罗马元老院和人民达成协议。选择曾试图在贝桑松袭击枢机主教罗兰的奥托，还有无法为儿子争取到拉文纳大主教地位的吉多作为特使，很明显巴巴罗萨并不很想向教宗示好。在罗马的事务上，巴巴罗萨准备将元老院的存在合法化，以此换取罗马人民接受巴巴罗萨任命的行省总督——根据隆卡利亚达成的法律，总督将作为皇帝在罗马城的首席代表，是帝国的podestà而不是教宗的代表。大使们没有达成任何一项目的。很难相信巴巴罗萨的这三位亲信特使在9月4日参加了阿德里安的葬礼，却没有在三天后亚历山大三世与维克多四世对于教宗之位的竞争之前，去暗中与枢机主教奥克塔维安还有圣彼得大教堂的主教们接触。另一方面值得注意的是，教宗亚历山大的传记作者枢机主教博索并没有指责奥托和吉多与9月7日的选举事件有直接的牵连。因此，使节们一定已经将巴巴罗萨对于阿德里安继任者人选的偏好公之于众了，并且还向奥克塔维安保证，如果他能够当选，皇帝就会支持他。然而，他们也一定会将这件事情交给枢机主教团和罗马人中的亲帝国派来经手，这样皇帝就可以理直气壮地否认自己直接参与干预了此事。

当巴巴罗萨在6月与枢机主教们谈判时，教宗阿德里安还有亲西西里一方的枢机主教还有威廉一世的其他使者离开了罗马，前往阿纳尼（Anagni）。在那里，据维克多四世的支持者记载，这群枢机主教也与米兰及其盟友有联系，他们在教宗面前发誓要把皇帝开除教籍，“至死不渝地反对他的意志与荣誉”，并且阿德里安许诺在他去世后只会从他们之中推举一个人继任教宗。米兰匿名者对1159年夏季发生的事件采取了亲亚历山大三世一方的口吻描述，这更印证了对他们反对帝国阴谋的指控。他记载，巴巴罗萨在7月开始围攻克雷马后，米兰人便于布雷西亚还有皮亚琴察结盟，并向位于阿纳尼的阿德里安派出特使。他们和克雷马人一同发誓，除非阿德里安或其合法继承人许可，不然他们不会与巴巴罗萨媾和或者达成任何协议。教宗同意了，但并没有回以誓言，只是表示将在接下来的40天内将皇帝开除教籍。然而他在真正实施这一行为之前就于1159年9月1日去世了。因此，不管有没有巴巴罗萨在背后的推波助澜，枢机主教团的分裂都几乎是注定的。​​​​
